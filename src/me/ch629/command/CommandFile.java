package me.ch629.command;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class CommandFile extends Command {
	@Override
	public CommandReturn run(List<String> lines, int callLine, String[] args) {
		if(args.length >= 1) {
			Path path = Paths.get(args[0]);
			try {
				lines.addAll(callLine, getFile(path));
			}catch(IOException e) {
				System.out.printf("Line %d || Invalid File Path for Command: FILE\n", callLine);
			}
		}else System.out.printf("Line %d || Invalid Arguments for Command: FILE\n", callLine);
		return null;
	}

	@Override
	public String getName() {
		return "FILE";
	}

	@Override
	public void onNextLine() {

	}

	@Override
	public CommandType getCommandType() {
		return CommandType.PreProcess;
	}

	private static List<String> getFile(Path filePath) throws IOException {
		return Files.readAllLines(filePath);
	}
}