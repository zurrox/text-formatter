package me.ch629.command;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CommandManager {
	public static List<Command> commands = new ArrayList<>();

	/**
	 * Get commands with a specific CommandType
	 *
	 * @param type - The CommandType
	 * @return All commands with a specific CommandType
	 */
	public static List<Command> getCommands(CommandType type) {
		return commands.stream().filter(cmd -> cmd.getCommandType() == type).collect(Collectors.toList());
	}

	public static Command getCommand(String name) {
		for(Command cmd : commands) if(cmd.getName().equals(name)) return cmd;
		return null;
	}

	public static Command getCommand(CommandType type, String name) {
		return getCommand(getCommands(type), name);
	}

	public static Command getCommand(List<Command> commands, String name) {
		for(Command cmd : commands) if(cmd.getName().equals(name)) return cmd;
		return null;
	}

	public static CommandReturn runCommand(List<Command> commands, List<String> lines, int callLine) {
		String[] args = lines.get(callLine).split(" ");
		Command command = getCommand(commands, args[0].substring(1));
		if(command != null) {
			args = lines.get(callLine).substring(args[0].length() + 1).split(" "); //Remove the command from the args
			return command.run(lines, callLine, args);
		}
		return null;
	}
}