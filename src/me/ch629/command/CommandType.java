package me.ch629.command;

public enum CommandType {
	PreProcess,
	InProcess,
	PostProcess
}