package me.ch629.command;

import java.util.HashMap;

public class CommandReturn {
	public HashMap<ReturnKeys, Object> data;

	public CommandReturn(Object... args) {
		data = new HashMap<>();

		if(args.length > 0 && args.length % 2 == 0) { //Key, Value, Key, Value etc
			for(int i = 0; i < args.length; i += 2) {
				String key = (String) args[i];
				Object value = args[i + 1];
			}
		}
	}

	public void setValue(ReturnKeys key, Object value) {
		if(data.containsKey(key)) data.remove(key);
		data.put(key, value);
	}

	public Object getObject(ReturnKeys key) {
		return data.containsKey(key) ? data.get(key) : null;
	}

	public String getString(ReturnKeys key) {
		return (String) getObject(key);
	}

	public int getInt(ReturnKeys key) {
		return (int) getObject(key);
	}

	public boolean getBoolean(ReturnKeys key) {
		return (boolean) getObject(key);
	}

	private void initialize() {
		for(ReturnKeys key : ReturnKeys.values()) if(! data.containsKey(key)) data.put(key, key.defaultValue);
	}
}