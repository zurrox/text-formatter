package me.ch629.command;

public enum ReturnKeys {
	LINES_ADDED("linesAdded", 0);
	public String key;
	public Object defaultValue;

	ReturnKeys(String key, Object defaultValue) {
		this.key = key;
		this.defaultValue = defaultValue;
	}
}