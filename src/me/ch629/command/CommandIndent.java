package me.ch629.command;

import me.ch629.TextFormatter;

import java.util.List;

public class CommandIndent extends Command {
	private boolean indented = false;
	int spaces;

	@Override
	public CommandReturn run(List<String> lines, int callLine, String[] args) {
		if(args.length >= 1) {
			this.spaces = Integer.parseInt(args[0]);
			TextFormatter.NEXT_INDENT = spaces;
			indented = true;
		}
		return null;
	}

	@Override
	public String getName() {
		return "IN";
	}

	@Override
	public void onNextLine() {
		if(indented) {
			indented = false;
			TextFormatter.NEXT_INDENT = 0;
		}
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.InProcess;
	}
}