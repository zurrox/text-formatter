package me.ch629.command;

import java.util.List;

public class CommandLineLength extends Command {
	@Override
	public CommandReturn run(List<String> lines, int callLine, String[] args) {
		return null;
	}

	@Override
	public String getName() {
		return "LL";
	}

	@Override
	public void onNextLine() {

	}

	@Override
	public CommandType getCommandType() {
		return CommandType.InProcess;
	}
}
