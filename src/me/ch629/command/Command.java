package me.ch629.command;

import java.util.List;

public abstract class Command {
	public Command() {
		CommandManager.commands.add(this);
	}

	/**
	 * Run the command with the specified arguments
	 *
	 * @param args - Arguments from the String after the command
	 */
	public abstract CommandReturn run(List<String> lines, int callLine, String[] args);

	/**
	 * @return The name of the command for reading
	 */
	public abstract String getName();

	/**
	 * Next text line which may need to be formatted doesn't run on commands
	 */
	public abstract void onNextLine();

	/**
	 * Whether to run the Command in Pre, In or Post(File's need to be inserted before processed)
	 *
	 * @return CommandType of the Command
	 */
	public abstract CommandType getCommandType();
}
