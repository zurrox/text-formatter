package me.ch629;

import me.ch629.command.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TextReader {
	public static List<String> lines;

	/**
	 * Read all input until terminated with .END
	 */
	public static void read() {
		Scanner in = new Scanner(System.in);
		lines = new ArrayList<>();
		in = in.useDelimiter("\n"); //TODO: Look into what this does
		while(in.hasNextLine()) {
			String line = in.nextLine();
			if(line.equals(".END")) {
				in.close();
				return;
			}
			lines.add(line);
		}
	}

	private static void process(List<Command> commands) {
		int lineNumber = 0, lineOffset = 0;

		List<String> tempLines = new ArrayList<>(lines);
		for(String line : tempLines) {
			if(line.startsWith(".")) { //Command
				CommandReturn cmdReturn = CommandManager.runCommand(commands, lines, lineNumber + lineOffset); //This may need to be changed with lineOffset and with TempLines so the lines can be read and then added to lines
				lineOffset += cmdReturn.getInt(ReturnKeys.LINES_ADDED);
			}
			lineNumber++;
		}
	}

	/**
	 * Inserts all files and macro's into the text
	 */
	public static void preProcess() {
		process(CommandManager.getCommands(CommandType.PreProcess));
	}

	public static void process() {
		process(CommandManager.getCommands(CommandType.InProcess));
	}

	public static void postProcess() {
		process(CommandManager.getCommands(CommandType.PostProcess));
	}
}